Example problem for using python [E"-binning package](https://gitlab.com/riekerlab/nutfit/tree/master/spectralfit_nonuniform).
*Furnace_Synth.asc* is calculated from 30-chunk tx_act_kelvin profile.
linelist/ contains Hitran-formatted linelist files (from [Schroeder database 2018](https://doi.org/10.1016/j.jqsrt.2018.02.025) ) for use with [hapi](https://hitran.org/hapi) .

data/ contains NTfit output files
*example_td.txt* is output you should get from running *Example_td.py*.

You can move this entire directory elsewhere on your computer if you wish.
Just need to find the ntfit package.