# -*- coding: utf-8 -*-
"""
Fit a simulated transmission spectrum for temperature distribution

INPUTS:
    - transmission spectrum (and corresponding x-axis)
    - HITRAN linelist
    - path-average TPXL to calculate absorption lineshapes
    - ranges of lower-state energy to fit as separate concentrations
OUTPUTS:
	- Spectral fit N.snorm_out and N.t_celsius
	- Temperature distribution L.tx[:,L.corner_all] and L.chi_fit

Created on Tue Jul 23 15:15:45 2019

@author: Nate Malarich
"""
# built-in modules
import numpy as np

# nonuniform-temperature code
import ntfit.spectrafit as ebin
from ntfit.tdist import Snorm2Tx
ebin.plt.rcParams['figure.autolayout'] = True

N = ebin.EbinHapi(r'data/furnace_example',
                  bandwidth = [6800, 7000])
N.linelist_path = r'linelist/H2O_PaulLF.data'

# Load transmission spectrum    
N.x_wvn_full, N.trans_full = np.loadtxt(r'data/Furnace_Synth.asc',
                                        skiprows = 18,
                                        unpack = True)
# Known path-average parameters
N.p_torr = 640 # from pressure sensor
N.pathlength = 152.4 # cm

# model 1% H2O in air
N.molec_id = 1
N.chi = 0.01

N.t_celsius = 200 # Optional: guess path-average temperature
N.ebin_edges = [0, 200, 500, 750, 1020, 1220, 1440, 1680, 1940,
                2220, 2510, 2820, 3100, 7000] # update default E"-bins
    
N.fit_snorm(); # the fastest fit, only float pathlength of each E"-bin.

# Refine fit lineshape by updating N.t_celsius, N.chi
N.fit_temperature(change_temperature=True, plot_se=False);
N.fit_snorm_width_shift(); # and scaling model cross-sections

# Now get temperature distribution from linestrengths
L = Snorm2Tx(N)
L.reg_weight = 10.**(np.arange(2,-5,-1))
f, ax = L.length_bin()

## Overplot the true temperature distribution used to simulate spectrum        
tx_act =  np.array([547, 581, 634, 699, 771, 843, 908, 959, 991, 996, 
            969, 903, 877, 822, 772, 724, 680, 638, 600, 564, 
            531, 500, 471, 443, 418, 395, 372, 351, 331, 312])
ax[1].plot(np.linspace(0,1,len(tx_act)), np.sort(tx_act), 
           'r--', label='act', lw=2)

if False:
    N.save_fit()
    N.__cleanup__() # Must repeat N.get_linelist_par() to make another fit
    L.save_fit()