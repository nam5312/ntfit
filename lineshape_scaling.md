NTfit v2.2 accuracy
16 August 2024

When you fit a spectrum with temperature nonuniformity, there will be some degree of model error in the lineshapes, (section 2.4 of PartII paper). The E"-binning code models the spectrum at a particular pressure, temperature, molefraction, and needs to rescale the model to fit the temperature-dependence of the Lorentz-widths and pressure-shifts.

To rescale these widths and shifts, Version 2.1.0 recalculates each Voigt lineshape at a different pressure and frequency-shift for each fit iteration. See `Ebin_td.spectra_float_width_shift()` in NTfit v2.1. Version 2.2 just rescales the original absorption cross-section with an x-interpolation (for the shift) and a time-domain decay-rate rescaling (for the Lorentz-width). This Lorentz-width rescaling is based on [Makowiecki JQSRT 2020](https://doi.org/10.1016/j.jqsrt.2020.107189).

In both versions, the lineshape-modelling occurs in the `spectra_float_width_shift()` function in `ebin_td.py`. The slow version 2.1 function calls `calc_spectrum_fd()` at a particular pressure and shift, whereas the version 2.2 function calls `shift_scale()` and `pressure_scale()`.

The newer v2.2 method provides much faster and more stable fits, while the v2.1 method is more accurate (although still an approximation because of variation in the temperature-exponents within each E"-bin). By "less stable fits", I mean that the old v2.1 fits must be initialized with a set of E"-bins each with enough absorption, AND must get the snorm close with a `fit_snorm` first; otherwise the shift fit will take 10 minutes and then not converge.

Why inaccurate?
- Different absorption lines in each E"-bin have different temperature-exponents, and we are fitting with an average
- x-interpolating a set of points will tend to reduce the integrated absorbance of the model. This effect is proportional to the ratio of frequency point-spacing to HWHM of the absorption feature.
- The Lorentz-width scaling ignores the Doppler (Gaussian) width of the Voigt lineshape. This ratio $\Gamma_{Lor}$ / $\Gamma_{Dop}$ is different for each line, and it also is smaller at lower pressures and higher temperatures.


### Testing accuracy

Take a synthetic spectrum from known temperature profile, FurnaceSynth.asc, and fit with the two different NTfit versions.

N.sdvoigt = False for this test: for Furnace_Synth.asc, and both v2.2 and v2.1. If adding sdvoigt, then the v2.1 fits would get substantially slower (because the speed-dependent lineshape takes longer to calculate in hapi1 than Voigt. [hapi2](https://github.com/hitranonline/hapi2) should mitigate this issue).

measurement at 200 MHz point spacing
gas is 1% H2O in air at P = 0.84 atm

so $\Gamma_{Lor}$ ~ 1000 MHz (down to 780 MHz at $T_{max}$ = 1000 K), 
$\Gamma_{Dop}$ ~ 450 MHz (up to 550 MHz at $T_{max}$ = 1000 K). (Linewidths $\Gamma$ are listed in half-width at half-maximum)

So frequency-interpolation error increases with $\Delta f$ / $\Gamma$ ~ 0.2

Pressure-scaling error increases with $\Gamma_{Dop}$ / $\Gamma_{Lor}$ = 0.5

The particular S(E") fits are all within 3% of the true integrated area. The low-E" fits are ~2% low and the high-E" fits are too-high normalized linestrength. For the slow v2.1, the high-E" fits are 1% too high, whereas for the fast v2.2, the high-E" fits are up to 3% too high. So the v2.2 approximation is slightly less accurate than the slower v2.1 method.

But the temperature inversion fits from those two spectral fits (see table below) both yield path-average tempeartures and molefractions within 1% of the simulation.


| param     | input | v2.2 | v2.1 |
| :--- | :---: |  :---: |  :---:  |
| t_avg (K) | 653.5 | 656  | 651  |
| t_max (K) | 996   | 927  | 906  |
| t_min (K) | 312   | 328  | 319  |
| fit time (s) | N/A|  12  | 360  |
| h2o (%)   | 1     | 1.00 | 0.99 |

And if I repeat the simulation with 25% the pressure (so there should be more pressure-scaling AND shift-scaling error from smaller $\Gamma_{Lor}$), the S(E") fits change by <0.2% for most E"-bins (maximum change -0.6% at E"=600). So NTfit v2.2 should be effective even for low-pressure spectra where the lineshape-scaling approximation is more coarse.