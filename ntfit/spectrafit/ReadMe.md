# Lower-state energy (E") binning package.
Python package to extract integrated areas from broadband spectrum.
Use like you would use *numpy*:
```
from ntfit import spectrafit as ebin
```

To use this package in Python, see [Example_td.py](https://gitlab.com/riekerlab/nutfit/blob/master/Example_files/Example_td.py) script.

Alternatively, you can run the following code and follow the instructions when you get assertion errors.
```
N = ebin.EbinHapi('test_ebin', bandwidth=[6800,7000]) # fit spectrum from 6800-7000 cm-1 and save results to test_ebin.txt
N.fit_snorm() # keep following instructions until this function runs.
```

## Description
This code extracts temperature nonuniformity across the laser path by splitting the H2O linelist into several groups of E" and fitting the concentrations of each subgroup.
The ensuing plot of "concentration" or "normalized linestrength" with respect to average lower-state energy of each group of H2O features has some curvature which is determined by the distribution of H2O molecules at different temperatures along the laser path.

This theory of the linestrength plots comes from Ref 1, and the absorption spectra are calculated using Hapi (Ref 2), and fitted using the mFID time-domain technique (Ref 3).

## Sources
If you use this package to fit linestrength curves, please cite the following:
 * 1 Malarich, Rieker "Resolving  non-uniform temperature distributions with single-beam absorption spectroscopy II) Implementation from broadband spectra." DOI: [10.1016/j.jqsrt.2021.107805](https://www.doi.org/10.1016/j.jqsrt.2021.107805)
 * 2 Kochanov et al "HITRAN Application Programming Interface (HAPI): A comprehensive approach to working with spectroscopic data." DOI: [10.1016/j.jqsrt.2016.03.005](https://doi.org/10.1016/j.jqsrt.2016.03.005)
 * 3 Makowiecki, Cole et al "Baseline-free Quantitative Absorption Spectroscopy Based on Cepstral Analysis." DOI: [10.1364/OE.27.037920](https://www.doi.org/10.1364/OE.27.037920).
