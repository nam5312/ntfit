# -*- coding: utf-8 -*-
"""
Helpful functions for time-domain fitting preparation.

td = time-domain

Created on Fri Nov  8 16:46:56 2019

@author: Nate the Average
"""

import numpy as np
from scipy.ndimage.filters import maximum_filter1d
SPEED_OF_LIGHT = 299792458

############################
## Get weighting function # 
###########################
def largest_prime_factor(n):
    i = 2
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
    return n


def bandwidth_select_td(x_array, band_fit, max_prime_factor = 100):
    '''
    Tweak bandwidth selection for swift time-domain fitting.
    
    Time-domain fit does inverse FFT for each nonlinear least-squares iteration,
    and speed of FFT goes with maximum prime factor.
    
    INPUTS:
        x_array = x-axis for measurement transmission spectrum
        band_fit = [start_frequency, stop_frequency]
    '''
    x_start = np.argmin(np.abs(x_array - band_fit[0]))
    x_stop = np.argmin(np.abs(x_array - band_fit[1]))
    if x_stop == len(x_array)-1:
        x_stop += 1
       
    len_td = 2 * (np.abs(x_stop - x_start) - 1) # np.fft.irfft operation
    prime_factor = largest_prime_factor(len_td)
    while prime_factor > max_prime_factor:
        x_stop -= 1
        len_td = 2 * (np.abs(x_stop - x_start) - 1)
        prime_factor = largest_prime_factor(len_td)
    return x_start, x_stop

def weight_func(len_fd, bl, etalons = []):
    '''
    Time-domain weighting function, set to 0 over selected baseline, etalon range
    INPUTS:
        len_fd = length of frequency-domain spectrum
        bl = number of points at beginning to attribute to baseline
        etalons = list of [start_point, stop_point] time-domain points for etalon spikes
    '''
    weight = np.ones(2*(len_fd-1))
    weight[:bl] = 0; weight[-bl+1:] = 0
    for et in etalons:
        weight[et[0]:et[1]] = 0
        weight[-et[1]+1:-et[0]+1] = 0
    return weight

def weight_func_ps(x_wvn, bl_ps, etalons_ps = []):
    '''
    Weighting function, scaled by effective time (picoseconds) not index
    '''
    len_fd = len(x_wvn)
    ti_ps = cepstrum_xaxis(x_wvn)[:len_fd]
    bl = np.argmin(np.abs(bl_ps - ti_ps))
    etalons = []
    for et in etalons_ps:
        st = np.argmin(np.abs(et[0] - ti_ps))
        en = np.argmin(np.abs(et[1] - ti_ps))
        etalons.append([st,en])
    weight = weight_func(len_fd, bl, etalons)
    return weight

def cepstrum_xaxis(x_data):
    '''
    Calculate x-axis scaling of time-domain cepstrum.
    In same units as x_data (the frequency-axis array)
    center data point should be (1/2)/frep,
    b/c double-sided interferogram, so maximum delay 1/2 point spacing
    first data point is DC
    INPUT:
        x_data (cm-1) array of frequencies
    OUTPUT:
        ti_ps (ps) array of cepstrum times (picoseconds)
    '''
    WVN2HZ = 100 * SPEED_OF_LIGHT
    dx_hz = (x_data[1] - x_data[0]) * WVN2HZ
    dx_thz = dx_hz / 1e12
    max_delay_ps = 1 / dx_thz / 2 # maximum delay is fr/2
    nx = len(x_data)
    ti_ps = np.linspace(0,1,nx) * max_delay_ps
    # wrap-around the symmetric second half of cepstrum
    ti_ps = np.concatenate((ti_ps, ti_ps[-2:0:-1]))
    
    return ti_ps


def bl_taylor(y_data_fd, y_mod_fd, p=10):
    '''Fit spectral residual with p-th order Taylor polynomial'''
    y_res = y_data_fd - y_mod_fd
    x0 = np.linspace(0, 1, len(y_res))
    poly_fd = np.polyval(np.polyfit(x0, y_res, p), x0)
    return poly_fd

def bl_remove_taylor(y_data_fd, y_mod_fd, p=10):
    '''Remove a low-order Taylor polynomial from the cepstrum.
    To reduce ringing on certain spectra'''
    bl_poly_fd = bl_taylor(y_data_fd, y_mod_fd, p)
    data_td = np.fft.irfft(y_data_fd - bl_poly_fd)
    return data_td

############################
## Noise stats # 
################
def est_snr(y_td):
    nt = len(y_td)
    signal = max(np.abs(y_td[-2]),np.abs(y_td[-1]))
    noise = np.std(y_td[nt//3:nt//2])
    return signal / noise

def calc_ss_noise(y_td, bl):
    '''
    Estimate sum-squared residuals due to absorbance noise.
    This is the lowest residual you could achieve wtih perfect fit
    '''
    nt = len(y_td)
    noise = np.std(y_td[nt//3:nt//2])
    n = nt - 2 * bl # remove baseline-filtered region
    ss = noise**2 * n
    
    return ss

def fit_quality(data_td, fit_td, weight_td, noise_filter = True, norm=1):
    '''
    Estimate fraction of absorption cepstrum fitted using 1-norm.
    '''
    w = weight_td.copy()
    if noise_filter:
        i_noise = noise_index(data_td)
        w[i_noise:-i_noise] = 0
    a_data = np.linalg.norm(w * data_td, norm)
    a_res = np.linalg.norm(w * (data_td - fit_td), norm)
    a_noise = calc_chi_noise(data_td, w)
    print('Fit residual is %.2fx noise floor and %.1f%% of data'
          % (a_res / a_noise, a_res / a_data * 100))
    print('At this weighting-window, signal is %.1fx noise'
          % (a_data / a_noise))

def calc_chi_noise(data_td, weight_td, norm=1):
    n2 = len(data_td) // 2
    n3 = int(.8 * n2)
    noise = np.std(data_td[n3:n2])
    nw = np.count_nonzero(weight_td)
    
    if norm == 1:
        return 0.8 * noise * nw
    elif norm == 2:
        return noise**2 * nw
    else:
        return False
    
def noise_index(data_td):
    '''
    Find time-index where cepstrum reaches noise floor
    '''
    roll_max = maximum_filter1d(data_td, 50)
    noise = roll_max[len(data_td) // 2] # noise-floor in middle of wraparound cepstrum
    return np.argmin(np.floor(roll_max / noise))
