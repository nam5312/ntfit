# -*- coding: utf-8 -*-
'''
Parse transmission spectra files (ASC)
and HITRAN-linelist files (DATA or PAR)
'''
import numpy as np
import matplotlib.pyplot as plt
import os # or pathlib
from time import strftime, localtime
try:
    from .td_support import bandwidth_select_td
except:
    from td_support import bandwidth_select_td
    
class Transmission():
    def __init__(self, bandwidth=[6800,7100]):
        
        # transmission spectrum information
        self.trans_full = None
        self.x_wvn_full = None
        self.wvn_start, self.wvn_stop = bandwidth
        self.transmission_path = '' # what file did transmission spectrum come from?
        
        # initialize thermodynamic parameters
        self.p_torr = 640
        self.t_celsius = 23
        self.chi = 0.01
        self.pathlength = 10 
                
    def get_transmission_asc(self, asc_file_path, overwrite_thermo=True):
        """
        Pulls important parameters from transmission file already formatted for Labfit
        
        INPUTS:
            asc_file_path = string name of transmission file (ignore .asc extension)
                            must be in labfit_engine directory else Labfit won't run
            self.wvn_start = fitting range for parameter
            
        OUTPUTS:
            Numpy arrays of frequency-domain transmission spectrum in object
            self.x_wvn = frequency axis (wavenumbers cm-1)
            self.trans_spectrum = intensity transmission (frequency-domain)
            
            Also pulls path-average thermo properties T,P,X,L from file
            self.pathlength
            self.t_celsius
            self.p_torr
            self.chi        
        """
        
        if '.asc' in asc_file_path:
        # Ignore .asc extension
            asc_file_path = asc_file_path[:-4]        
        self.transmission_path = os.path.abspath(asc_file_path) + '.asc'
        
        ## First extract path-average thermodynamic parameters
        if overwrite_thermo:
            with open(asc_file_path + '.asc','r') as f:
                for i in range(3):
                    f.readline()
                therm_line = f.readline()
                path_m, t_celsius, p_torr, chi = therm_line.split()[:4]
                self.pathlength = float(path_m) * 100 # cm
                self.t_celsius = float(t_celsius)
                self.p_torr = float(p_torr)
                self.chi = float(chi)

        ## Then get transmission spectrum
        try:
            self.x_wvn, self.trans_spectrum = np.loadtxt(asc_file_path + '.asc',unpack=True, skiprows=18)
        except ValueError:
            self.x_wvn, self.trans_spectrum, res = np.loadtxt(asc_file_path + '.asc',unpack=True, skiprows=18)
        # last update the subset bandwidth
        # Check that transmission spectrum is large enough
        if self.x_wvn[0] > self.wvn_start:
            self.wvn_start = np.ceil(self.x_wvn[0])
            print('Bandwidth too large for transmission file.')
            print('Changing self.wvn_start = ', self.wvn_start)
        if self.x_wvn[-1] < self.wvn_stop:
            self.wvn_stop = np.floor(float(self.x_wvn[-1]))
            print('Bandwidth too large for transmission file.')
            print('Changing self.wvn_stop = ', self.wvn_stop)
        
        self.x_wvn_full = self.x_wvn.copy()
        self.trans_full = self.trans_spectrum.copy()
        self.select_bandwidth()
        
    def select_bandwidth(self):
        ''' Adjust array to match fit window with fast FFT for time-domain''' 
        band_fit = [self.wvn_start, self.wvn_stop]
        x_start, x_stop = bandwidth_select_td(self.x_wvn_full, band_fit)
        self.x_wvn = self.x_wvn_full[x_start:x_stop]
        self.trans_spectrum = self.trans_full[x_start:x_stop]
    
    def plot_transmission(self):
        '''Plot transmission spectrum with fit-region inset'''
        plt.figure()
        plt.plot(self.x_wvn_full, self.trans_full, color='#cbcbcbff')
        plt.plot(self.x_wvn, self.trans_spectrum, 'k')
        plt.xlabel('Wavenumber (cm$^{-1}$)')
        plt.ylabel('Intensity (a.u.)')
    
    def make_asc(self, file_name = 'Spectrum007',
                 message = 'Data for E"-bin time-domain fitting', save_res = False):
        """
        Make Labfit-formatted transmission text file.
        18 lines of header, then space-delimited array x_wvn, trans
        
        INPUTS:
            self.trans_spectrum = laser transmission spectrum (y-axis numpy array)
            self.x_wvn = (cm-1) wavenumber of each data point (numpy array)
            self.p_torr, self.t_celsius, self.pathlength, self.chi = path-average properties
            file_name = name of output ASC file for Labfit
            save_res: if True, save absorbance residual in 3rd column (from self.spectrum_by_ebin())
            
        OUTPUTS:
            transFile.asc = Labfit-formatted transmission spectrum
            
        ASSUMPTIONS:
            self.wvn_start and self.wvn_stop are where you want them
            Actually Labfit can handle an ASC file larger than the fitting region
        
        """
        
        self.transmission_path = os.path.abspath(file_name) + '.asc' # !! possible bug: remove .txt or other file extension

        ## Format strings for ASC file conventions
        self.wvn_step = (self.x_wvn[-1] - self.x_wvn[0]) / (len(self.x_wvn) - 1) # :31f
        # Change T,P,X,L to formatted strings
        pathlength = self.pathlength/100; T_celsius = self.t_celsius; p_torr = self.p_torr
        if(pathlength < 10):
            pathlength = '{0:.7f}'.format(pathlength)
        elif(pathlength < 100):
            pathlength = '{0:.6f}'.format(pathlength)
        if(T_celsius < 100): # Assume T_celsius > 10c
            T_celsius = '{0:.4f}'.format(T_celsius)
        elif(T_celsius < 1000):
            T_celsius = '{0:.3f}'.format(T_celsius)
        else:
            T_celsius = '{0:.2f}'.format(T_celsius)
        if(p_torr < 10):
            p_torr = '{0:.5f}'.format(p_torr)
        elif(p_torr < 100):
            p_torr = '{0:.4f}'.format(p_torr)
        elif(p_torr < 1000):
            p_torr = '{0:.3f}'.format(p_torr)
        elif(p_torr < 10000):
           p_torr = '{0:.2f}'.format(p_torr)
        else:
            p_torr = '{0:.1f}'.format(p_torr)
        chi = '{0:.6f}'.format(self.chi)
        
        ## Now write to output file
        with open(self.transmission_path,'w') as out_file:
            file_id = '1111'
            out_file.write('******* File ' + file_id + ', ' + message + '\n')
            out_file.write(file_id+'   ')
            out_file.write(f'{self.x_wvn[0]:.10f}' + '  ')
            out_file.write(f'{self.x_wvn[-1]:.8f}'  + '  ')
            out_file.write( f'{self.wvn_step:.31f}' + '\n')
            out_file.write('  00000.00    0.00000     0.00e0     ')
            out_file.write('1   2     3   0        0\n') # Hitran molecule codes
            # Thermo line
            out_file.write(4*' ' + pathlength + 5*' ' + T_celsius + 6*' ' + 
                           p_torr + 4*' ' + chi + '    .0000000 .0000000 0.000\n')
            for i in range(2):
                out_file.write('    0.0000000     23.4486      0.00000    0.000000')
                out_file.write('    .0000000 .0000000 0.000\n')
            for i in range(9):
                out_file.write('1. 1. 1. 1. 1. 1. 1. 1. 1. 1.\n')
        # Other introductory lines
#            out_file.write('DATE ' + date + '; time ' + time + '\n')
            out_file.write(strftime("%m/%d/%Y %H:%M:%S\n", localtime()))
            out_file.write('\n')
            out_file.write('6801.3716254 0.003338200000000000000000000000 15031')
            out_file.write(' 1 1    2  0.9935  0.000   0.000 294.300 295.400')
            out_file.write(' 295.300   7.000  22.000 500.000 START\n')
         # Now write out transmission spectrum
            for count, vals in enumerate(self.x_wvn):
                out_file.write(f'{vals:.5f}' + 6*' ')
                out_file.write(f'{self.trans_spectrum[count]:.5f}')
                if save_res:
                    res_i = self.data_fd_blfilt[count] - self.absorption_total[count]
                    out_file.write(6*' ' + f'{res_i:.5f}')
                out_file.write('\n')





'''
Pull data from HITRAN linelist file into Pandas dataframe
'''
import json
from pandas import DataFrame

HITRAN_160 = {
        # Formatting guide to pull from PAR files to be consistent with hapi
        'molec_id': {'index':(0,2), 'par_format': '%2d',
                     'labfit_id': 0, 'labfit_format': '%3d',
                     'description': 'Hitran indicator, 1 for H2O'},
        'local_iso_id': {'index':(2,3), 'par_format':'%1d',
                         'labfit_id': 1, 'labfit_format': '%2d',
                         'description': 'isotopologue, almost always 1'},
        'nu': {'index':(3,15), 'par_format': '%12.6f',
               'labfit_id': 2, 'labfit_format': '%14.7f',
               'description': 'linecenter (cm-1)'},
        'sw': {'index':(15,25), 'par_format': '%10.3E',
               'labfit_id': 3, 'labfit_format': '%13.5E',
               'description': ' S296 (cm-1/atm)'},
        'gamma_air': {'index':(35,40), 'par_format': '%5.4f',
                      'labfit_id': 4, 'labfit_format': '%10.5f',
                      'description': 'foreign-broadening (cm-1/atm)'},
        'gamma_self': {'index':(40,45), 'par_format': '%5.3f',
                       'labfit_id': 10, 'labfit_format': '%10.5f',
                       'description': 'self-broadening half-width-half-max (cm-1/atm)'},
        'elower': {'index': (45,55), 'par_format': '%10.4f',
                   'labfit_id': 5, 'labfit_format': '%14.7f',
                   'description': 'lower-state energy E" (cm-1)'},
        'n_air': {'index': (55,59), 'par_format': '%4.2f',
                  'labfit_id': 6, 'labfit_format': '%8.4f',
                  'description': 'foreign-broadening temperature exponent'},
        'delta_air': {'index': (59,67), 'par_format': '%8.6f',
                      'labfit_id': 7, 'labfit_format': '%11.7f',
                      'description': 'foreign-shift (cm-1/atm)'},
        'quanta': {'index': (67, 127), 'par_format': '%60s',
                   'labfit_id': 17, 'labfit_format': '%60s', # for 4-line Labfit
                   'description': 'quantum numbers for upper-vibration, lower-vibration, upper-rotation, lower-rotation'}
        }
        
HITRAN_160_EXTRA = {
        'ierr': {'index':[127,133], "default": "   EEE"},
        'iref': {'index':[133,145], "default": "         EEE"},
        'other': {'index':[145,160], "default": 'E    0.0    0.0'}
        }

def par_to_df(hit_file, nu_min = 0, nu_max = 10000):
    '''
    Turn standard par file into dataframe with hapi names.
    '''                
    # Look for extra parameters in header file
    extra = []
    header_file = hit_file.split('.')[0] + '.header'
    if os.path.exists(header_file):
        with open(header_file,'r') as header:
            head = json.loads(header.read())
        try:
            extra = head['extra']
            extra_separator = head['extra_separator']
        except KeyError:
            pass
    with open(hit_file,'r') as air_linelist:
        linelist = []
        nu_i = HITRAN_160['nu']['index']
        for hit_line in air_linelist.readlines():
            # 100x faster to make list of dictionaries than append rows to dataframe
            row_dict = {}
            if len(hit_line) > 1:
                if float(hit_line[nu_i[0]:nu_i[1]]) < nu_min:
                    continue
                elif float(hit_line[nu_i[0]:nu_i[1]]) > nu_max:
                    break
                for name, props in HITRAN_160.items():
                    value = hit_line[props['index'][0]:props['index'][1]]
                    param_type = props['par_format'][-1]
                    if param_type == 's':
                        row_dict[name] = value
                    elif param_type == 'd':
                        row_dict[name] = int(value)
                    else:
                        try:
                            row_dict[name] = float(value)
                        except ValueError:
                            row_dict[name] = 0.0
                    if 'quanta' in name:
                        row_dict['quanta_index'] = ' '.join(value.split())
                for name, props in HITRAN_160_EXTRA.items():
                    row_dict[name] = hit_line[props['index'][0]:props['index'][1]]
                if len(extra) > 0:
                    extras = hit_line.split(extra_separator)[1:]
                    for name, value in zip(extra, extras):
                        row_dict[name] = float(value)
                linelist.append(row_dict)
        titles = list(HITRAN_160.keys())
        titles.append('quanta_index')
        titles.extend(list(HITRAN_160_EXTRA.keys()))
        titles.extend(extra)
        df_air = DataFrame(linelist, columns = titles)
#        df_air['Smax'] = df_air['sw']*(.518*df_air['elower']/296)**(-2.77) * np.exp(-1.435 * df_air['elower']*(1/(.518 * df_air['elower']) - 1/296))
    return df_air