# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 11:26:39 2022

@author: nam8
"""

import numpy as np

def lsq(A,y, w = None):
    '''
    Least-squares, solve system of equations for x 
    min ||w (y - Ax)||
    
    INPUTS:
        y = measurements
        A = matrix
        w = weighting matrix for each element,
            default is identity matrix

    OUTPUTS:
        x = fit array
        sigma_x = statistical uncertainty in each entry of x
        rho_s = nondimensional correlations between each entry of x
        
    '''
    
    cov_s = np.linalg.inv(A.T @ A)
    x = cov_s @ A.T @ y
    
    # Get 1-sigma uncertainty
    res = y - A @ x
    sigma_x = get_uc(cov_s, x, res)
    rho_s = correls(cov_s)# get fractional correlation matrix
    
    return x, sigma_x, rho_s
    
def get_uc(cov_s, x, res):
    chi2 = np.sum(res**2) / (len(res) - len(x))
    sigma_x = np.sqrt(np.diag(cov_s) * chi2)
    
    return sigma_x

def correls(cov_s):
    '''
    Turn covariance matrix into non-dimensionalized correlations 
    (-1 < rho < 1)
    '''
    rho_s = cov_s.copy()
    for i in range(np.shape(cov_s)[0]):
        rho_s[:,i] /= np.sqrt(cov_s[i,i])
        rho_s[i,:] /= np.sqrt(cov_s[i,i])
        rho_s[i,i] = 0 # should be 1, but set to 0 to emphaize GUI color-code
    return rho_s

def uc_leastsq(out):
    '''
    Get 1-sigma uncertainties from scipy.optimize.leastsq() result.
    Must have full_output = True
    '''
    cov_x = out[1]
    if cov_x is None:
        print('Fit did not converge')
    else:
        x = out[0]        
        res = out[2]['fvec']
        sigma_x = get_uc(cov_x, x, res)        
        return sigma_x
       