from . import hapi
from . import tdist
from . import spectrafit

__version__ = "2.2.2"
__author__ = 'Nathan Malarich'
__credits__ = "University of Colorado Boulder"