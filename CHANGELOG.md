# Change Log

## [2.2.2] - 2024-08-16
- less buggy handling of full spectrum bandwidth vs subrange for time-domain fit

## [2.2.1] -2024-08-15
- Added molefraction-column result to temperature inversion

## [2.2] - 2024-08-10
### Changed
- `fit_snorm_width_shift()` scales Lorentz-width and shift of original model, rather than recalculating Voigt lineshapes each iteration. Huge speed and stability improvements at a small cost of accuracy for E"-bins having lines with varied air-lineshape coefficients. 
- `p_rat` a Lorentz-scaling, not a pressure-ratio. Values << 1, detect magnitude or saved fits for backwards compatibility.
- changed several variable names to be (hopefully) more intuitive and following Python style guide
- Absorption models calculated further into wings
### Added
- r-squared fit statistics to ntfit.spectrafit


## [2.1] - 2022-09-23
### Changed
- ntfit now a separate sub-package, compatible with pip install
- `hapi` sub-package has separate file for Q(T) tables
- Removed uniform_fit example, as lookup table is much more efficient way to fit temperature
- Tikhonov regularization module now `ntfit.tdist`
- E"-bin spectral-fitting now `ntfit.spectrafit`

## [2.0] - 2021-03-01
### Added
- E"-bin spectral fitting package with hapi